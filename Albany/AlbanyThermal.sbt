<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">

  <Categories Default="Heat 3D">
    <Cat>Heat 3D</Cat>
  </Categories>

  <Analyses>
    <Analysis Type="Heat 3D">
      <Cat>Heat 3D</Cat>
    </Analysis>
  </Analyses>

  <Definitions>
    <AttDef Type="Problem" BaseType="" Version="0" Unique="true" Associations="">
      <ItemDefinitions>
	<String Name="Problem Type" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1" Optional="false" IsEnabledByDefault="true">
          <BriefDescription>Problem type</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Heat 3D">Heat 3D</Value>
          </DiscreteInfo>
	</String>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Discretization Properties" BaseType="" Version="0" Unique="true" Associations="">
      <ItemDefinitions>
	<Void Name="Separate Evaluators by Element Block" Version="0" AdvanceLevel="1"  Unique="true"  Optional="true" IsEnabledByDefault="true"/>
      </ItemDefinitions>
    </AttDef>


    <AttDef Type="Thermal Conductivity" BaseType="" Version="0" Unique="true" Associations="">
      <ItemDefinitions>
	<String Name="Thermal Conductivity Type" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1" Optional="false" IsEnabledByDefault="true">
          <BriefDescription>Global vs local thermal conductivities</BriefDescription>
          <DiscreteInfo DefaultIndex="0">
	    <!--
		<Value Enum="Constant">Constant</Value>
	    -->
            <Value Enum="Block Dependent">Block Dependent</Value>
          </DiscreteInfo>
          <Categories>
            <Cat>Heat 3D</Cat>
          </Categories>
	</String>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Response Function" BaseType="" Version="0" Unique="false">
      <ItemDefinitions>

	<String Name="Response Function Type" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1" Optional="false" IsEnabledByDefault="true">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="Solution Average">Solution Average</Value>
            <Value Enum="Solution 2-Norm">Solution 2-Norm</Value>
            <Value Enum="Solution Values">Solution Values</Value>
            <Value Enum="Solution Max Value">Solution Max Value</Value>
            <Value Enum="Solution Min Value">Solution Min Value</Value>
          </DiscreteInfo>
	</String>

	<String Name="Response Output File" Label="Output File" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
	</String>

	<String Name="Culling Strategy" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1" Optional="true" IsEnabledByDefault="false">
          <ChildrenDefinitions>
            <ModelEntity Name="NodeSet" Label="Node Set" Version="0"
			 AdvanceLevel="0" NumberOfRequiredValues="1">
              <MembershipMask>volume</MembershipMask>
            </ModelEntity>
            <Int Name="Node GIDs" Version="0" Nodal="true" Unique="false"
		 Extensible="true" NumberOfRequiredValues="1">
              <BriefDescription>The GID list used for culling.</BriefDescription>
	    </Int>
          </ChildrenDefinitions>
          <DiscreteInfo>
	    <Structure>
              <Value Enum="Node Set">Node Set</Value>
	      <Items>
		<Item>NodeSet</Item>
	      </Items>
	    </Structure>
	    <Structure>
            <Value Enum="Node GIDs">Node GIDs</Value>
	    <Items>
	      <Item>Node GIDs</Item>
	    </Items>
	    </Structure>
          </DiscreteInfo>
	</String>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Boundary Condition" BaseType="" Abstract="1" Version="0" Unique="false" Associations="">
      <ItemDefinitions>

        <Double Name="Value" Label="Value" Version="0" NumberOfRequiredValues="1">
          <DefaultValue>0</DefaultValue>
        </Double>

        <String Name="DOF" Label="Degree of Freedom" Version="0" NumberOfRequiredValues="1">
          <DefaultValue>T</DefaultValue>
	</String>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Dirichlet Boundary Condition" Label="Dirichlet BC" BaseType="Boundary Condition" Version="0" Unique="true" Nodal="true" Associations="v|e|f">
      <ItemDefinitions>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Neumann Boundary Condition" Label="Neumann BC" BaseType="Boundary Condition" Version="0" Unique="true" Nodal="true" Associations="v|e|f">
      <ItemDefinitions>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Material" BaseType="" Version="0" Unique="true" Associations="r">
      <ItemDefinitions>
        <String Name="Category" Label="Material Type" Version="0" NumberOfRequiredValues="1"/>

        <Double Name="Thermal Conductivity" Label="Thermal Conductivity (W/(m*K))" Version="0" NumberOfRequiredValues="1">
          <DefaultValue>0</DefaultValue>
        </Double>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Reference Material" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <AttributeRef Name="Material" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <AttDef>Material</AttDef>
        </AttributeRef>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Solver Parameters" BaseType="" Version="0" Unique="true" Associations="">
      <ItemDefinitions>
        <Double Name="Convergence Tolerance" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <DefaultValue>1.e-5</DefaultValue>
        </Double>

        <Int Name="Maximum Iterations" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <DefaultValue>100</DefaultValue>
        </Int>

        <Int Name="Number of Blocks" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <DefaultValue>50</DefaultValue>
        </Int>

        <Int Name="Block Size" Version="0" AdvanceLevel="1" NumberOfRequiredValues="1">
          <DefaultValue>1</DefaultValue>
        </Int>

      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Solver Verbosity" BaseType="" Version="0" Unique="true" Associations="">
      <ItemDefinitions>
        <Int Name="Output Frequency" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <DefaultValue>10</DefaultValue>
        </Int>

        <Int Name="Output Style" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <DiscreteInfo DefaultIndex="1">
            <Value Enum="General">0</Value>
            <Value Enum="Brief">1</Value>
          </DiscreteInfo>
        </Int>

        <Int Name="Verbosity" Version="0" AdvanceLevel="0" NumberOfRequiredValues="1">
          <DefaultValue>33</DefaultValue>
        </Int>

      </ItemDefinitions>
    </AttDef>

  </Definitions>

  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Group" Title="SimBuilder" TopLevel="true" TabPosition="East">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <Views>
	<View Title="Problem Definition"/>
	<View Title="Boundary Conditions"/>
	<View Title="Materials"/>
	<View Title="Response Functions"/>
	<View Title="Solver Parameters"/>
      </Views>
    </View>

    <View Type="Instanced" Title="Problem Definition">
      <InstancedAttributes>
        <Att Name="Problem" Type="Problem"/>
        <Att Name="Discretization Properties" Type="Discretization Properties"/>
        <Att Name="Thermal Conductivity" Type="Thermal Conductivity"/>
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="Boundary Conditions" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Name="Boundary Condition" Type="Boundary Condition"/>
      </AttributeTypes>
    </View>

    <View Type="Group" Title="Materials" Style="Tiled">
      <Views>
        <View Title="Material Types" />
        <View Title="Reference Material" />
      </Views>
    </View>

    <View Type="Attribute" Title="Material Types" ModelEntityFilter="r" CreateEntities="true">
      <AttributeTypes>
        <Att Name="Material" Type="Material"/>
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Reference Material">
      <InstancedAttributes>
        <Att Name="Reference Material" Type="Reference Material"/>
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="Response Functions" ModelEntityFilter="r" CreateEntities="true">
      <AttributeTypes>
        <Att Name="Response Function" Type="Response Function"/>
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="Solver Parameters">
      <InstancedAttributes>
        <Att Name="Solver Parameters" Type="Solver Parameters"/>
        <Att Name="Solver Verbosity" Type="Solver Verbosity"/>
      </InstancedAttributes>
    </View>

  </Views>
</SMTK_AttributeSystem>

#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================

import smtk
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET

class ParameterList:
  def __init__(self, parent_=None, name_=""):
    if parent_:
      self.element = ET.SubElement(parent_.element,
                                 "ParameterList",name=name_)
    else:
      self.element = ET.Element("ParameterList")

class Parameter:
  def __init__(self, parent_, name_, type_, value_):
    self.element = ET.SubElement(parent_.element,
                                 "Parameter",name=name_,type=type_,value=value_)

def AddProblemInput(root,spec):
  problem_par = ParameterList(root,"Problem")

  manager = spec.getSimulationAttributes()

  problem_def_att = manager.findAttribute('Problem')
  problem_type = smtk.attribute.to_concrete(
    problem_def_att.find("Problem Type"))
  problem_type_par = Parameter(problem_par,"Name","string",
                               problem_type.valueAsString())

  export_att = spec.getExportAttributes().findAttributes('ExportSpec')[0]
  materials_file = smtk.to_concrete(export_att.find('Albany Materials File')).value(0)
  material_par = Parameter(problem_par,"MaterialDB Filename","string",materials_file)

  dirichlet_bcs_par = ParameterList(problem_par,"Dirichlet BCs")
  for bc in manager.findAttributes("Dirichlet Boundary Condition"):
    model_ent_item = bc.associations()
    if model_ent_item.numberOfValues() == 0:
      continue
    for i in range(model_ent_item.numberOfValues()):
      entity = model_ent_item.value(i)
      entity_name = "nodeset_%d" % entity.integerProperty("pedigree id")[0]

      dirichlet_bc_par = Parameter(dirichlet_bcs_par,
                                   "DBC on NS nodeset_" + entity_name + " for DOF "+bc.findString("DOF").value(0),
          "double",
          str(bc.findDouble("Value").value(0)))

  thermal_conductivity_att = manager.findAttribute('Thermal Conductivity')
  thermal_conductivity_type = smtk.attribute.to_concrete(
    thermal_conductivity_att.find("Thermal Conductivity Type"))
  thermal_conductivity_par = ParameterList(problem_par,"Thermal Conductivity")
  thermal_conductivity_type = Parameter(
    thermal_conductivity_par,"Thermal Conductivity Type","string",
    thermal_conductivity_type.valueAsString())

  response_fns_par = ParameterList(problem_par,"Response Functions")
  response_fns = manager.findAttributes("Response Function")
  response_count_par = Parameter(response_fns_par,"Number","int",
                                 str(len(response_fns)))
  response_fn_index = 0
  for response_fn in response_fns:
    response_par = Parameter(response_fns_par,"Response %d" % response_fn_index,
                             "string", response_fn.findString(
                               "Response Function Type").value(0))
    response_parameter_type = response_fn.findString("Culling Strategy")
    if response_parameter_type:
      response_parameters_par = ParameterList(response_fns_par,
                                        "ResponseParams %d" % response_fn_index)
      response_parameters_type_par = Parameter(response_parameters_par,
                                               "Culling Strategy",
                                               "string",
                                               response_parameter_type.value(0))
      if response_parameter_type.value(0) == "Node GIDs":
        response_nodes = response_fn.findInt("Node GIDs")

        if not response_nodes:
          msg = 'WARNING: response \'%s\' has \'Node GID List\' assignment but no nodes.' % response_fn.name()
          print msg
          spec.getLogger().addWarning(msg)
          continue

        node_gid_list = "{%s" % str(response_nodes.value(0))
        for i in range(response_nodes.numberOfValues()-1):
            node_gid_list += ",%s" % str(response_nodes.value(i+1))
        node_gid_list += "}"
        response_parameter_node_array_par = Parameter(response_parameters_par,
                                                      "Node GID Array",
                                                      "Array(int)",
                                                      node_gid_list)
      else:
        model_ent_item = bc.associations()
        if model_ent_item.numberOfValues() == 0:
          msg = 'WARNING: response \'%s\' has \'Node Set\' assignment but no node set.' % response_fn.name()
          print msg
          spec.getLogger().addWarning(msg)
          continue
        entity = model_ent_item.value(0)
        response_parameter_node_set_par = Parameter(response_parameters_par,
                                                    "Node Set Label",
                                                    "string",
                                                    entity.name())
    response_output_file = response_fn.findString("Response Output File")
    if response_output_file:
      response_output_file_par = Parameter(response_parameters_par,
                                           "Output File",
                                           "string",
                                           response_output_file.value(0))

    response_fn_index += 1

def AddMaterialsInput(root,spec):
  manager = spec.getSimulationAttributes()
  element_blocks_par = ParameterList(root,"ElementBlocks")
  materials_par = ParameterList(root,"Materials")

  reference_material = manager.findAttribute("Reference Material").findRef(
                                                  "Material").value(0)
  if reference_material:
    reference_material_par = Parameter(root,"Reference Material","string",
                                       reference_material.name())
  else:
    msg = 'WARNING: no reference material assgined.'
    print msg
    spec.getLogger().addWarning(msg)

  element_blocks = set()

  for material in manager.findAttributes("Material"):
    model_ent_item = material.associations()
    if model_ent_item.numberOfValues() == 0:
      continue
    material_par = ParameterList(materials_par,material.name())
    material_category_par = Parameter(material_par,"Category","string",
                                      material.findString("Category").value(0))
    material_tc_par = ParameterList(material_par,"Thermal Conductivity")
    material_const_par = Parameter(material_tc_par,"Name","string","Constant")
    material_value_par = Parameter(material_tc_par,"Value","double",
                      str(material.findDouble("Thermal Conductivity").value(0)))

    for i in range(model_ent_item.numberOfValues()):
      entity = model_ent_item.value(i)
      if i != 0 and entity.name in element_blocks:
        msg = 'WARNING: volume \'%s\' has more than one material assigned to it.' % entity.name()
        print msg
        spec.getLogger().addWarning(msg)
        continue

      entity_name = "block_%d" % entity.integerProperty("pedigree id")[0]
      material_assign_par = ParameterList(element_blocks_par,entity_name)
      material_assign_mat_par = Parameter(material_assign_par,"material",
                                          "string",material.name())

def AddDiscretizationInput(root,spec):
  manager = spec.getSimulationAttributes()
  if manager is not None:
    model_manager = manager.refModelManager()
    export_att = spec.getExportAttributes().findAttributes('ExportSpec')[0]
    model_ents = model_manager.entitiesMatchingFlags(smtk.model.MODEL_ENTITY,
                                                     True)
    if not model_ents:
      msg = 'No model - cannot export'
      print 'WARNING:', msg
      spec.getLogger().addWarning(msg)
      print 'Abort export - check logger'
      return False
    elif len(model_ents) > 1:
      msg = 'Multiple models - using first one'
      print 'WARNING:', msg
      spec.logger.addWarning(msg)
    model_ent = model_ents.pop()

  urls = model_manager.stringProperty(model_ent, 'url')
  if urls:
      model_file = urls[0]

  output_file = smtk.to_concrete(export_att.find('Exodus Output File')).value(0)

  discretization_par = ParameterList(root,"Discretization")
  disc_method_par = Parameter(discretization_par,"Method","string","Exodus")
  model_input_file_par = Parameter(discretization_par,"Exodus Input File Name",
                                   "string",model_file)
  model_output_file_par = Parameter(discretization_par,
                                    "Exodus Output File Name",
                                    "string",
                                    output_file)
  disc_att = manager.findAttribute('Discretization Properties')
  separate = smtk.attribute.to_concrete(
    disc_att.find("Separate Evaluators by Element Block"))
  choice = "true" if separate.isEnabled() else "false"
  separate_par = Parameter(discretization_par,
                           "Separate Evaluators by Element Block",
                           "bool",choice)

def AddSolverInput(root,spec):
  manager = spec.getSimulationAttributes()

  # for now, we hard-code the Block GMRES solver
  piro_par = ParameterList(root,"Piro")
  nox_par = ParameterList(piro_par,"NOX")
  direction_par = ParameterList(nox_par,"Direction")
  dir_newton_par = Parameter(direction_par,"Method","string","Newton")
  newton_par = ParameterList(direction_par,"Newton")
  stratimikos_linear_solver_par = ParameterList(newton_par,
                                                "Stratimikos Linear Solver")
  stratimikos_par = ParameterList(stratimikos_linear_solver_par,
                                  "Stratimikos")
  belos_solver_par = Parameter(stratimikos_par,"Linear Solver Type",
                               "string","Belos")

  linear_solvers_par = ParameterList(stratimikos_par,"Linear Solver Types")
  belos_par = ParameterList(linear_solvers_par,"Belos")
  block_gmres_type_par = Parameter(belos_par,
                                   "Solver Type","string","Block GMRES")
  solver_types_par = ParameterList(belos_par,"Solver Types")
  block_gmres_par = ParameterList(solver_types_par,"Block GMRES")

  solver_params_att = manager.findAttribute('Solver Parameters')
  solver_verbose_att = manager.findAttribute('Solver Verbosity')

  tolerance = smtk.attribute.to_concrete(
      solver_params_att.find("Convergence Tolerance"))
  tolerance_par = Parameter(block_gmres_par,"Convergence Tolerance","double",
                            str(tolerance.value(0)))

  output_freq = smtk.attribute.to_concrete(
      solver_verbose_att.find("Output Frequency"))
  output_freq_par = Parameter(block_gmres_par,"Output Frequency","int",
                              str(output_freq.value(0)))

  output_style = smtk.attribute.to_concrete(
      solver_verbose_att.find("Output Style"))
  output_style_par = Parameter(block_gmres_par,"Output Style","int",
                               str(output_style.value(0)))

  verbosity = smtk.attribute.to_concrete(
      solver_verbose_att.find("Verbosity"))
  verbosity_par = Parameter(block_gmres_par,"Verbosity","int",
                            str(verbosity.value(0)))

  max_iterations = smtk.attribute.to_concrete(
      solver_params_att.find("Maximum Iterations"))
  max_iterations_par = Parameter(block_gmres_par,"Maximum Iterations","int",
                                 str(max_iterations.value(0)))

  block_size = smtk.attribute.to_concrete(
      solver_params_att.find("Block Size"))
  block_size_par = Parameter(block_gmres_par,"Block Size","int",
                             str(block_size.value(0)))

  num_blocks = smtk.attribute.to_concrete(
      solver_params_att.find("Number of Blocks"))
  num_blocks_par = Parameter(block_gmres_par,"Num Blocks","int",
                             str(num_blocks.value(0)))
  flexible_gmres_par = Parameter(block_gmres_par,"Flexible Gmres","bool","0")

  preconditioner_type_par = Parameter(stratimikos_par,"Preconditioner Type",
                                      "string","Ifpack")
  preconditioner_types_par = ParameterList(stratimikos_par,
                                           "Preconditioner Types")
  ifpack_par = ParameterList(preconditioner_types_par,"Ifpack")
  overlap_par = Parameter(ifpack_par,"Overlap","int","1")
  prec_type_par = Parameter(ifpack_par,"Prec Type","string","ILUT")
  ifpack_settings_par = ParameterList(ifpack_par,"Ifpack Settings")
  drop_tolerance_par = Parameter(ifpack_settings_par,"fact: drop tolerance",
                                 "double","0")
  ilut_level_of_fill_par = Parameter(ifpack_settings_par,
                                     "fact: ilut level-of-fill","double","1.0")
  level_of_fill_par = Parameter(ifpack_settings_par,"fact: level-of-fill",
                                "int","1")

def ExportCMB(spec):
  '''
  Entry function, called by CMB to write export file
  '''

  root = ParameterList()
  materials_root = ParameterList()

  AddProblemInput(root,spec)
  AddMaterialsInput(materials_root,spec)
  AddDiscretizationInput(root,spec)
  AddSolverInput(root,spec)

  export_att = spec.getExportAttributes().findAttributes('ExportSpec')[0]
  materials_file = smtk.to_concrete(export_att.find('Albany Materials File')).value(0)
  with open(materials_file, 'w') as file:
    file.write(parseString(ET.tostring(materials_root.element)).toprettyxml(indent = "  "))

  albany_input_file = smtk.to_concrete(export_att.find('Albany Input File')).value(0)
  with open(albany_input_file, 'w') as file:
    file.write(parseString(ET.tostring(root.element)).toprettyxml(indent = "  "))
